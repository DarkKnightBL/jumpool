///create_button(w,h,spr,b_txt,b_col,x,y,ref,ev,dep,spr_alt,b_sc,sc,anim)
tmp_obj = instance_create(argument5,argument6,obj_par_button)
tmp_obj.w = argument0
tmp_obj.h = argument1
tmp_obj.spr = argument2
tmp_obj.b_txt = argument3
tmp_obj.b_col = argument4
tmp_obj.ref = argument7
tmp_obj.ev = argument8
tmp_obj.depth = argument9
tmp_obj.spr_alt = argument10
tmp_obj.anchor_x = argument5
tmp_obj.anchor_y = argument6
tmp_obj.b_sc = argument11
tmp_obj.sc = argument12
tmp_obj.anim = argument13
with(tmp_obj)
{
    event_perform(ev_other,ev_user0)
}
return (tmp_obj)
