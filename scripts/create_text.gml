///create_text(x,y,b_txt,b_col,sc,dep)
txt_obj = instance_create(argument0,argument1,obj_par_text)
txt_obj.b_txt = argument2
txt_obj.b_col = argument3
txt_obj.sc = argument4
txt_obj.depth = argument5
txt_obj.anchor_x = argument0
txt_obj.anchor_y = argument1
return (txt_obj)
